#include "../catch.hpp"

#include "DataStream.h"

TEST_CASE("DataStream", "[DataStream]")
{
	remove("testData/data.txt");
	isam::BinaryStream<isam::DataRecord> creator("testData/data.txt"); 

	for (int i = 0; i < 21; i++)
		creator.write_at_end( 
			{ i,
			static_cast<float>(1.57), 
			static_cast<float>(i/1.5),
			static_cast<float>(i/1.9),
			0 } );

	isam::DataStream dataStream("testData/data.txt");
	
	isam::DataRecord test1 = dataStream.find_record(10, 14);

	REQUIRE(Approx(test1.key) == 14);
	REQUIRE(Approx(test1.a) == 1.57);
	REQUIRE(Approx(test1.b) == 14/1.5);
	REQUIRE(Approx(test1.c) == 14/1.9);
	REQUIRE(Approx(test1.overflow) == 0);
	
	test1 = dataStream.find_record(0, 6);
	REQUIRE(Approx(test1.key) == 6);
	REQUIRE(Approx(test1.a) == 1.57);
	REQUIRE(Approx(test1.b) == 6 / 1.5);
	REQUIRE(Approx(test1.c) == 6 / 1.9);
	REQUIRE(Approx(test1.overflow) == 0);
}



TEST_CASE("Add Record with full pages", "[DataStream]")
{
	remove("testData/DStestData1.txt");
	isam::BinaryStream<isam::DataRecord> dataCreator("testData/DStestData1.txt");
	isam::DataRecord data[]{{ 0,0,0,0,0 },	//0 
							{ 1,1,1,1,0 }, /*page 0 || key 1*/
							{ 2,1,1,1,0 },  //1
							{ 3,1,1,1,0 },  //2
							{ 4,1,1,1,0 },
							{ 5,1,1,1,0 },  //4
							{ 6,1,1,1,0 },
							{ 11,1,1,1,0 },  //6
							{ 12,1,1,1,0 },
							{ 13,1,1,1,0 },  //8
							{ 14,1,1,1,0 },  //9
							{ 21,1,1,1,0 }, /*page 1 || key 11*/
							{ 22,1,1,1,0 },
							{ 23,1,1,1,0 },
							{ 24,1,1,1,0 },
							{ 25,1,1,1,0 },
							{ 26,1,1,1,0 },
							{ 27,1,1,1,0 },
							{ 28,1,1,1,0 },
							{ 29,1,1,1,0 },
							{ 30,1,1,1,0 } };

	for each(auto rec in data)
		dataCreator.write_at_end(rec);

	SECTION("Find Chain")
	{
		isam::DataStream dataStream("testData/DStestData1.txt");
		//REQUIRE(5 == dataStream.find_overflow_pivot_position(0, 8));
		//REQUIRE(9 == dataStream.find_overflow_pivot_position(10, 15));
		//REQUIRE(9 == dataStream.find_overflow_pivot_position(10, 19));
		//REQUIRE(5== dataStream.find_overflow_pivot_position(0, 7));
	}

	SECTION("Adding Tests") {
		isam::DataStream dataStream("testData/DStestData1.txt");
		dataStream.add_record(8,1,1,1,1);
		dataStream.add_record(15,1,1,1,1);
		dataStream.add_record(7,1,1,1,1);
		REQUIRE(dataCreator.read_at(21).key == 8  );
		REQUIRE(dataCreator.read_at(22).key == 15 );
		REQUIRE(dataCreator.read_at(23).key == 7  );
		REQUIRE(dataCreator.read_at(21).overflow == 0);
		REQUIRE(dataCreator.read_at(6).overflow == 23);
		REQUIRE(dataCreator.read_at(11).key == 21);
		REQUIRE(dataCreator.read_at(23).overflow == 21);
	}
}

TEST_CASE("Add Record with empty pages", "[DataStream]")
{
	remove("testData/DStestData1.txt");
	isam::BinaryStream<isam::DataRecord> dataCreator("testData/DStestData1.txt");
	isam::DataRecord data[]{ 
		{ 0,0,0,0,0 },	//0
		{ 1,1,1,1,0 },  //1 
		{ 2,1,1,1,0 },  //2 
		{ 3,1,1,1,0 },  //3 
		{ 4,1,1,1,0 },  //4 
		{ 5,1,1,1,0 },  //5 
		{ 6,1,1,1,0 },  //6 
		{ 0,0,0,0,0 },  //7 
		{ 0,0,0,0,0 },  //8 
		{ 0,0,0,0,0 },  //9 
		{ 0,0,0,0,0 },  //10 
		{ 11,1,1,1,0 }, //11 
		{ 12,1,1,1,0 },	//12 
		{ 13,1,1,1,0 },	//13 
		{ 14,1,1,1,0 },	//14 
		{ 17,1,1,1,0 },	//15 
		{ 18,1,1,1,0 },	//16 
		{ 0,0,0,0,0 },	//17 
		{ 0,0,0,0,0 },	//18 
		{ 0,0,0,0,0 },	//19 
		{ 0,0,0,0,0 },	//20 
		{ 31,1,1,1,0 }, //21 
		{ 32,1,1,1,0 },	//22 
		{ 33,1,1,1,0 },	//23 
		{ 34,1,1,1,0 },	//24 
		{ 35,1,1,1,0 },	//25 
		{ 36,1,1,1,0 },	//26 
		{ 0,0,0,0,0 },	//27 
		{ 0,0,0,0,0 },	//28 
		{ 0,0,0,0,0 },	//29 
		{ 0,0,0,0,0 } };//30 
						 
	for each(auto rec in data)
		dataCreator.write_at_end(rec);

	SECTION("Find Chain")
	{
		isam::DataStream dataStream("testData/DStestData1.txt");
//		REQUIRE(6 == dataStream.find_overflow_pivot_position(0, 8));
//		REQUIRE(13 == dataStream.find_overflow_pivot_position(10, 15));
//		REQUIRE(16 == dataStream.find_overflow_pivot_position(10, 19));
//		REQUIRE(6 == dataStream.find_overflow_pivot_position(0, 7));
	}

	SECTION("Adding Tests") {
		isam::DataStream dataStream("testData/DStestData1.txt");
		dataStream.add_record(7, 1, 1, 1, 1);
		dataStream.add_record(8, 1, 1, 1, 1);
		dataStream.add_record(15, 1, 1, 1, 11); 
		dataStream.add_record(16, 1, 1, 1, 11);
		dataStream.add_record(19, 1, 1, 1, 11);
		dataStream.add_record(20, 1, 1, 1, 11);
		dataStream.add_record(21, 1, 1, 1, 11);
		dataStream.add_record(22, 1, 1, 1, 11);
		dataStream.add_record(24, 1, 1, 1, 11);
		dataStream.add_record(23, 1, 1, 1, 11);

		REQUIRE(dataCreator.read_at(9).key == 0);
		REQUIRE(dataCreator.read_at(8).key == 8);
		REQUIRE(dataCreator.read_at(7).key == 7);
		REQUIRE(dataCreator.read_at(14).key == 14);
		REQUIRE(dataCreator.read_at(14).overflow == 31);
		REQUIRE(dataCreator.read_at(20).key == 22);
		REQUIRE(dataCreator.read_at(20).overflow == 34);

		REQUIRE(dataCreator.read_at(31).key == 15);
		REQUIRE(dataCreator.read_at(31).overflow == 32);
		REQUIRE(dataCreator.read_at(32).key == 16);
		REQUIRE(dataCreator.read_at(32).overflow == 0);
		REQUIRE(dataCreator.read_at(33).key == 24);
		REQUIRE(dataCreator.read_at(33).overflow == 0);
		REQUIRE(dataCreator.read_at(34).key == 23);
		REQUIRE(dataCreator.read_at(34).overflow == 33);

	}
}

TEST_CASE("Adding to Sentinel","[DataStream]") 
{

	remove("testData/DStestData2.txt");
	isam::BinaryStream<isam::DataRecord> dataCreator("testData/DStestData2.txt");
	isam::DataRecord data[]{
		{ 0,0,0,0,0 },	//0
		{ 11,1,1,1,0 },  //1 
		{ 12,1,1,1,0 },  //2 
		{ 13,1,1,1,0 },  //3 
		{ 14,1,1,1,0 },  //4 
		{ 15,1,1,1,0 },  //5 
		{ 16,1,1,1,0 },  //6 
		{ 0,0,0,0,0 },  //7 
		{ 0,0,0,0,0 },  //8 
		{ 0,0,0,0,0 },  //9 
		{ 0,0,0,0,0 },  //10 
		{ 21,1,1,1,0 }, //11 
		{ 22,1,1,1,0 },	//12 
		{ 23,1,1,1,0 },	//13 
		{ 24,1,1,1,0 },	//14 
		{ 27,1,1,1,0 },	//15 
		{ 28,1,1,1,0 },	//16 
		{ 0,0,0,0,0 },	//17 
		{ 0,0,0,0,0 },	//18 
		{ 0,0,0,0,0 },	//19 
		{ 0,0,0,0,0 },	//20 
	};

	for each(auto rec in data)
		dataCreator.write_at_end(rec);

	SECTION("Adding to Sentinel")
	{
		isam::DataStream dataStream("testData/DStestData2.txt");
		dataStream.add_record(1, 0, 1, 0, 0);
		dataStream.add_record(2, 0, 1, 0, 0);
		dataStream.add_record(7, 0, 1, 0, 0);
		dataStream.add_record(3, 0, 1, 0, 0);
		dataStream.add_record(4, 0, 1, 0, 0);
		dataStream.add_record(17, 0, 1, 0, 1);
		dataStream.add_record(29, 0, 1, 0, 11);
		
		REQUIRE(dataCreator.read_at(0).overflow == 21 );
		REQUIRE(dataCreator.read_at(21).overflow == 22 );
		REQUIRE(dataCreator.read_at(22).overflow == 24 );
		REQUIRE(dataCreator.read_at(23).overflow == 0 );
		REQUIRE(dataCreator.read_at(24).overflow == 25 );
		REQUIRE(dataCreator.read_at(25).overflow == 23 );
		REQUIRE(dataCreator.read_at(6).overflow == 0);
		REQUIRE(dataCreator.read_at(7).overflow == 0);
		REQUIRE(dataCreator.read_at(16).overflow == 0);
		REQUIRE(dataCreator.read_at(17).overflow == 0);
		REQUIRE(dataCreator.read_at(0). key == 0);
		REQUIRE(dataCreator.read_at(21).key == 1);
		REQUIRE(dataCreator.read_at(22).key == 2);
		REQUIRE(dataCreator.read_at(23).key == 7);
		REQUIRE(dataCreator.read_at(24).key == 3);
		REQUIRE(dataCreator.read_at(25).key == 4);
		REQUIRE(dataCreator.read_at(6).key == 16);
		REQUIRE(dataCreator.read_at(7).key == 17);
		REQUIRE(dataCreator.read_at(16).key== 28);
		REQUIRE(dataCreator.read_at(17).key== 29);
	}

}