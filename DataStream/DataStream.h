#pragma once

#include <vector>
#include "../BinaryStream/BinaryStream.h"
#include "../DataRecord/DataRecord.h"

namespace isam{
	class DataStream
	{
		BinaryStream<DataRecord> file;
		std::vector<DataRecord> page;
		int const pageSize;

		void update_page_buffer(int pagePos);
		int find_overflow_pivot_position(int pagePos, int key);
		void add_at_overflow_chain_end_position(int initialPos, DataRecord newOne);
		void insert_between(DataRecord inserted, DataRecord previous, int previousPos);
	public:
		DataStream(std::string);
		~DataStream();
		
		void add_record(int key, float a, float b, float c, int pivotPage);

		DataRecord find_record(int pagePos, int key);

	};
}