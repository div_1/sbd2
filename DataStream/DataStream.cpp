#include "DataStream.h"
#include <iostream>
#include <iomanip>
#include <limits>
namespace isam {
	DataStream::DataStream(std::string name)
		:file(name), pageSize(10)
	{
		page.resize(10);
		for (int i = 0; i < 10; i++)
			page[i] = { 0,0,0,0,0 };
	}

	DataStream::~DataStream()
	{
	}


	void DataStream::update_page_buffer(int pagePos)
	{
		this->file.read_at(pagePos, this->page);
	}

	void DataStream::add_record(int key, float a, float b, float c, int pagePos)
	{
		int pivotPos = find_overflow_pivot_position(pagePos, key);
		if (pivotPos == -1)
			std::cout << "Duplicate Record Trial" << std::endl;
		else
			add_at_overflow_chain_end_position(pivotPos, { key,a,b,c,0 });
	}

	void DataStream::insert_between(DataRecord inserted, DataRecord previous, int previousPos)
	{
		inserted.overflow = previous.overflow;
		previous.overflow = file.get_size();
		file.write_at_end(inserted);
		file.write_at(previousPos, previous);
	}

	void DataStream::add_at_overflow_chain_end_position(int previousPos, DataRecord newOne)
	{ //It was hard to write, it should be hard to read
		DataRecord previous = file.read_at(previousPos);
		DataRecord next;
		while (true) {
			if (previous.key == 0 && previousPos != 0) {
				file.write_at(previousPos, newOne);
				break;
			}
			if (previous.overflow == 0) {
				insert_between(newOne, previous, previousPos);
				break;
			}
			next = file.read_at(previous.overflow);
			if (next.key == newOne.key) {
				std::cout << "Duplicate key" << std::endl;
				break;
			}
			if (next.key > newOne.key) {
				insert_between(newOne, previous, previousPos);
				break;
			}
			previousPos = previous.overflow;
			previous = next;
		}
	}

	int DataStream::find_overflow_pivot_position(int pagePos, int key)
	{
		update_page_buffer(pagePos);
		for each(auto record in this->page) {
			if (pagePos == pagePos + pageSize)
				return pagePos;
			if (record.key == key)
				return -1;
			if (record.key > key)
				return --pagePos;
			if (record.key == 0) {
				return pagePos;
			}
			pagePos++;
		}
		pagePos--;
		return pagePos;
	}

	DataRecord DataStream::find_record(int pagePos, int key)
	{
		update_page_buffer(pagePos);
		for (int i = 0; i < page.size(); i++) {
			if (page[i].key == key)
				return page[i];
			if (i < page.size() - 1 && page[i + 1].key > key) {
				if (page[i].overflow != 0) {
					DataRecord current = file.read_at(page[i].overflow);
					while (true) {
						if (current.key == key)
							return current;
						else if (current.overflow == 0)
							return{ 0,7,7,7,0 };
						else {
							current = file.read_at(current.overflow);
						}
					}
				}
			}
		}
		return{ 0,0,7,0,0 };
	}
}