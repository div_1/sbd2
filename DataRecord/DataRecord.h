#pragma once

#include <iostream>
#include <iomanip>
namespace isam {
	class DataRecord
	{
	public:
		int key;
		float a, b, c;
		int overflow;
		void print() {
			std::cout << std::setw(0);
			std::cout << "# " << std::setfill(' ') << std::setw(3) << " [ ";
			std::cout << std::setfill(' ') << std::setw(3) << std::right;
			std::cout << key;
			std::cout << std::setw(0);
			std::cout << " ]" << "[ ";
			std::cout << std::setw(3);
			std::cout << overflow;
			std::cout << std::setw(0);
			std::cout << " ] " << std::endl;
		}
		bool operator==(const DataRecord& other) {
			if (other.a != this->a)
				return false;
			if (other.b != this->b)
				return false;
			if (other.c != this->c)
				return false;
			if (other.key != this->key)
				return false;
			if (other.overflow != this->overflow)
				return false;
		}
	};
}
