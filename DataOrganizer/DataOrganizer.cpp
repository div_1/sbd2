#include "DataOrganizer.h"


namespace isam {
	DataOrganizer::DataOrganizer(std::string name)
		:file(name), 
		temporary("tmp.temporary"), 
		overflowBorder( std::numeric_limits<int>::max() ),
		nextPosition(0),
		fileSize(file.get_size()),
		bufferIndex(0),
		filename(name)
	{
		temporary.reset();
		temporary.write_at(0, { 0,0,0,0,0 });
		buffer.resize(10);
		for (int i = 0; i < 10; i++)
			buffer[i] = { 0,0,0,0,0 };

	}


	DataOrganizer::~DataOrganizer()
	{
		if(bufferIndex != 0)
			flush();
		temporary.close();
		file.close();
		remove(filename.c_str());
		rename("tmp.temporary", filename.c_str());
		
	}

	void DataOrganizer::flush() {
		DataRecord empty = { 0,0,0,0,0 };
		if( bufferIndex != 0){
			bufferIndex = 0;
			temporary.write_at_end(buffer);
			for (int i = 0; i < 10; i++)
				buffer[i] = empty;
		}
	}

	void DataOrganizer::add_to_buffer(DataRecord rec)
	{
		buffer[bufferIndex] = { rec.key,rec.a,rec.b,rec.c,0 };
		bufferIndex++;
		if (bufferIndex > 6) {
			flush();
		}
	}

	void DataOrganizer::reorganize()
	{

		DataRecord sentinel = file.read_at(0);
		if(sentinel.overflow != 0){
			write_overflow_chain(sentinel.overflow);
		}
		nextPosition = 1;
		while (true) {

			std::vector<DataRecord> old_page(10);
			if (file.get_size() - nextPosition < 10)
				old_page.resize(file.get_size() - nextPosition);

			file.read_at(nextPosition, old_page);
			for (int i = 0; i < old_page.size(); i++){
				DataRecord temp = old_page[i];//file.read_at(nextPosition);
				if (temp.key != 0) {
					add_to_buffer(temp);
					if(temp.overflow != 0){
						write_overflow_chain(temp.overflow);
					}
				}
				nextPosition++;
				if (nextPosition >= overflowBorder)
					break;
				if (nextPosition >= file.get_size())
					break;
			}
			if (nextPosition >= overflowBorder)
				break;
			if (nextPosition >= file.get_size())
				break;
		}
	}
	void DataOrganizer::write_overflow_chain(int next) 
	{
		while (true) {
			if (next < overflowBorder)
				overflowBorder = next;

			DataRecord temp = file.read_at(next);
			next = temp.overflow;
			add_to_buffer({temp.key, temp.a, temp.b, temp.c, 0});
			if (next == 0)
				break;
		}
	}
}