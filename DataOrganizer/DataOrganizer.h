#pragma once

#include "../DataRecord/DataRecord.h"
#include "../BinaryStream/BinaryStream.h"

namespace isam {

	class DataOrganizer
	{
		BinaryStream<DataRecord> file;
		BinaryStream<DataRecord> temporary;

		int overflowBorder;
		int nextPosition;
		int fileSize;
		int bufferIndex;
		std::vector<DataRecord> buffer;
		std::string filename;
	public:
		DataOrganizer(std::string name);
		~DataOrganizer();

		void flush();
		void add_to_buffer(DataRecord rec);
		void reorganize();
		void write_overflow_chain(int next);

	};

}
