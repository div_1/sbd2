#include "../catch.hpp"
#include "DataOrganizer.h"
#include "../DataRecord/DataRecord.h"
#include "../DataStream/DataStream.h"

TEST_CASE("Reorganizning test","[DataOrganizer]")
{
	remove("testData/DataOrganizerTemp.txt");
	remove("testData/DOTests.txt");
	isam::BinaryStream<isam::DataRecord> creator("testData/DOTests.txt");
	isam::DataRecord data[] =
	{ 
		{ 0,0,0,0,21 }, //0
		{ 4,1,1,1,0 },  //1  //p0
		{ 5,1,1,1,0 },	//2
		{ 7,1,1,1,0 },	//3
		{ 8,1,1,1,0 },	//4
		{ 9,1,1,1,0 },	//5
		{ 10,1,1,1,23 },	//6
		{ 0,1,1,1,0 },	//7
		{ 0,1,1,1,0 },	//8
		{ 0,1,1,1,0 },	//9
		{ 0,0,0,0,0 }, //10 
		{ 13,1,1,1,0 },  //11  //p1
		{ 14,1,1,1,0 },	//12
		{ 16,1,1,1,0 },	//13
		{ 17,1,1,1,0 },	//14
		{ 20,1,1,1,0 },	//15
		{ 21,1,1,1,0 },	//16
		{ 22,1,1,1,0 },	//17
		{ 23,1,1,1,0 },	//18
		{ 0,0,0,0,0 },	//19
		{ 0,0,0,0,0 },  //20
		{ 1,1,1,1,22 },  //21  //p2
		{ 2,1,1,1,0 },	//22
		{ 11,1,1,1,24 },	//23
		{ 12,1,1,1,0 },	//24
	};
	for each (auto rec in data)
		creator.write_at_end(rec);

	SECTION("First Test") {
		isam::DataOrganizer reorg("testData/DOTests.txt");
		reorg.reorganize();
		reorg.flush();
		
		isam::DataStream stream("testData/DOTests.txt");
		//stream.print_whole();

	}
}