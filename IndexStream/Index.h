#pragma once
#include "IndexRecord.h"
#include "../BinaryStream/BinaryStream.h"

namespace isam {
		int find_page(std::string indexFileName, int key);
		void data_to_index(std::string dataFileName, std::string indexFileName);
}