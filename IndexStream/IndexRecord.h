#pragma once
#include <iomanip>
#include <iostream>
namespace isam {

	class IndexRecord {
	public:
		int key;
		int page;
		void print() {
			std::cout << "# " << std::setfill(' ') << std::setw(3) << " [ ";
			std::cout << std::setfill(' ') << std::setw(3) << std::right;
			std::cout << key;
			std::cout << std::setw(0);
			std::cout << " ]" << "[ ";
			std::cout << std::setw(3);
			std::cout << page;
			std::cout << std::setw(0);
			std::cout << " ] " << std::endl;
		}

	};
}