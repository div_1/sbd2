#include "Index.h"
#include "../catch.hpp"
#include "../DataRecord/DataRecord.h"


TEST_CASE("Finding proper page", "[IndexStream]")
{
	std::remove("testData/IStest.txt");
	isam::IndexRecord testPages[] = { { 1,1 },
									{ 30,2 },
									{ 55,3 },
									{ 100,4 },
									{ 117,5 } };
	isam::BinaryStream<isam::IndexRecord> creator("testData/IStest.txt");

	for (int i = 0; i < 5; i++)
		creator.write_at_end(testPages[i]);

	REQUIRE(find_page(creator,5) == 1 );
	REQUIRE(find_page(creator,35) == 2);
	REQUIRE(find_page(creator,65) == 3);
	REQUIRE(find_page(creator,115) == 4);
	REQUIRE(find_page(creator,200) == 5);
}

TEST_CASE("Mapping File", "[IndexStream]")
{
	
	remove("testData/IStestData.txt");
	remove("testData/IStestIndex.txt");
	isam::BinaryStream<isam::DataRecord> dataCreator("testData/IStestData.txt");
	
	isam::DataRecord datas[50];
	for (int i = 0; i < 50; i++)
		datas[i] = { i , static_cast<float>(i), static_cast<float>(i), static_cast<float>(i), 0 };

	for (int i = 0; i < 50; i++)
		dataCreator.write_at_end(datas[i]);
	{
		
		isam::data_to_index("testData/IStestData.txt", "testData/IStestIndex.txt");
	}

	isam::BinaryStream<isam::IndexRecord> dataStream("testData/IStestIndex.txt");
	REQUIRE(dataStream.read_at(0).key == 1 );
	REQUIRE(dataStream.read_at(1).key == 11);
	REQUIRE(dataStream.read_at(2).key == 21);
	REQUIRE(dataStream.read_at(3).key == 31);
	REQUIRE(dataStream.read_at(4).key == 41);

	REQUIRE(dataStream.read_at(0).page == 1);
	REQUIRE(dataStream.read_at(1).page == 11);
	REQUIRE(dataStream.read_at(2).page == 21);
	REQUIRE(dataStream.read_at(3).page == 31);
	REQUIRE(dataStream.read_at(4).page == 41);
}