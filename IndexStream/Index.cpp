#include "Index.h"
#include "../BinaryStream/BinaryStream.h"
#include "../DataRecord/DataRecord.h"
#include "IndexRecord.h"

namespace isam {

	int find_page(std::string indexFileName, int key)
	{
		BinaryStream<IndexRecord> indexFile(indexFileName);
		int prevKey = 0;
		for (int i = 0; i < indexFile.get_size(); i+=5) {
				std::vector<IndexRecord> page(5);
			if( indexFile.get_size() - i < 5){
				page.resize(indexFile.get_size() - i);
			}
			indexFile.read_at(i, page);
			for (int j = 0; j < page.size(); j++){
				if (page[j].key > key) {
					return prevKey;
				}
				prevKey = page[j].page;
			}
		}
		return prevKey;
	}

	void data_to_index(std::string dataFileName, std::string indexFileName)
	{
		BinaryStream<DataRecord> dataFile("data_file.data");
		BinaryStream<IndexRecord> indexFile("index_file.index");
		indexFile.reset();

		std::vector<IndexRecord> page(5);
		int page_index = 0;
		indexFile.write_at(0, { dataFile.read_at(1).key, 1 });

		for (int i = 2; i < dataFile.get_size(); i++) {
			if (i % 10 == 1){
				DataRecord current = dataFile.read_at(i);
				page[page_index] = { current.key, i };
				page_index++;
			}
			if (page_index >= 5) {
				page_index = 0;
				indexFile.write_at_end(page);
			}
		}
		page.resize(page_index);
		indexFile.write_at_end(page);
	}
	/*	void data_to_index(std::string dataFileName, std::string indexFileName)
	{
		BinaryStream<DataRecord> dataFile("data_file.data");
		BinaryStream<IndexRecord> indexFile("index_file.index");
		indexFile.reset();

		std::vector<IndexRecord> page(5);
		int page_index = 0;
		indexFile.write_at(0, { dataFile.read_at(1).key, 1 });

		for (int i = 2; i < dataFile.get_size(); i++) {
			if (i % 10 == 1){
				DataRecord current = dataFile.read_at(i);
				indexFile.write_at_end({ current.key, i });
			}
		}
	}*/
}