#pragma once
#include "../DataRecord/DataRecord.h"
#include "../BinaryStream/BinaryStream.h"
class DataRecord;

namespace isam{
	class Printer
	{
		BinaryStream<DataRecord> file;
		int nextPosition;
		int overflowBorder;
		void print_overflow_chain(int next);
		void print_record(DataRecord rc);
	public:
		Printer(std::string name);
		~Printer();
		void print_sorted();

	};
}

