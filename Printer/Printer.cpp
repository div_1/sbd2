#include "Printer.h"
#include <limits>
#include <iostream>
#include <iomanip>
#include "../DataRecord/DataRecord.h"
#include "../BinaryStream/BinaryStream.h"

namespace isam{
	Printer::Printer(std::string name)
		:file(name),
		nextPosition(0),
		overflowBorder(std::numeric_limits<int>::max())
	{
	}


	Printer::~Printer()
	{
	}
	void Printer::print_sorted()
	{
		DataRecord sentinel = file.read_at(0);
		if (sentinel.overflow != 0) {
			print_overflow_chain(sentinel.overflow);
		}
		nextPosition = 1;
		while (true) {
			DataRecord temp = file.read_at(nextPosition);
			if (temp.key != 0) {
				print_record(temp);
				if (temp.overflow != 0) {
					print_overflow_chain(temp.overflow);
				}
			}
			nextPosition++;
			if (nextPosition >= overflowBorder)
				break;
			if (nextPosition >= file.get_size())
				break;
		}
	}

	void Printer::print_overflow_chain(int next)
	{
		while (true) {
			if (next < overflowBorder)
				overflowBorder = next;
			DataRecord temp = file.read_at(next);
			next = temp.overflow;
			print_record(temp);
			if (next == 0)
				break;
		}
	}
	void Printer::print_record(DataRecord rc){
		std::cout << std::left;
		std::cout << "[Key: " << std::setw(4) << rc.key << std::setw(0) << "] ";
		std::cout << "[A: " << std::setw(4) << rc.a << std::setw(0) << "] ";
		std::cout << "[B: " << std::setw(4) << rc.b << std::setw(0) << "] ";
		std::cout << "[C: " << std::setw(4) << rc.c << std::setw(0) << "] ";
		std::cout << "[Ptr: " << std::setw(4) << rc.overflow << std::setw(0) << "] " << std::endl;
		std::cout << std::right;
	}
}