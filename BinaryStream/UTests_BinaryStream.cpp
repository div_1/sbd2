#include "../catch.hpp"
#include "BinaryStream.h"
#include <type_traits>

class Test {
public:
	char a, b, c;
	bool operator==(const Test& other) {
		if (this->a != other.a)
			return false;
		if (this->b != other.b)
			return false;
		if (this->c != other.c)
			return false;
		return true;
	}
};


TEST_CASE("UTests_BinaryStream", "BinaryStream.h") 
{
	remove("testData/BStest.txt");
	isam::BinaryStream<Test> stream("testData/BStest.txt");
	Test t1[3] = { { 1,2,3},{ 0,0,0 },{ 4,5,6 } };
	Test t4{ 12,13,14 };

	
	SECTION("write_at && read_at") {
		for (int i = 0; i < 3; i++) {
			stream.write_at(i, t1[i]);
			t4 = stream.read_at(i);
			REQUIRE(t4 == t1[i]);
		}
	}
	SECTION("Throwing from wrong position") {
		REQUIRE_THROWS(t4 = stream.read_at(10));
		REQUIRE_THROWS(t4 = stream.read_at(-10));
		REQUIRE_THROWS(stream.write_at(100, t4));
	
	}
	SECTION("write_at_end && read_at") {
		for (int i = 0; i < 3; i++) {
			stream.write_at(i, t1[i]);
			t4 = stream.read_at(i);
			REQUIRE(t4 == t1[i]);
		}
	}
	SECTION("write_at && read_at") {
		for (int i = 0; i < 3; i++) {
			stream.write_at_end(t1[i]);
			t4 = stream.read_at(i);
			REQUIRE(t4 == t1[i]);
		}
	}
	SECTION("write_at && read_at") {
		for (int i = 0; i < 3; i++) {
			REQUIRE(stream.get_size() == i);
			stream.write_at_end(t1[i]);
			t4 = stream.read_at(i);
			REQUIRE(t4 == t1[i]);
			REQUIRE(stream.get_size() == i+1);
		}
	}
	SECTION("reading whole page") {
		for (int i = 0; i < 3; i++) 
			stream.write_at_end(t1[i]);
		std::vector<Test> page;
		page.resize(3);

		stream.read_at(0, page);
		for (int i = 0; i < 3; i++)
			REQUIRE(page[i] == t1[i]);

	}
	SECTION("Replacing") {
		for (int i = 0; i < 3; i++)
			stream.write_at_end(t1[i]);
		stream.write_at(1, t4);
		Test t5 = stream.read_at(1);
		REQUIRE(t4.a == t5.a);
		REQUIRE(t4.b == t5.b);
		REQUIRE(t4.c == t5.c);
	}
	SECTION("Write Vector at end") {
		std::vector<Test> vr(4);
		vr[0] = { 1,2,3 };
		vr[1] = { 3,4,5 };
		vr[2] = { 6,7,8 };
		vr[3] = { 0,0,0 };
		stream.write_at_end(vr);
		stream.write_at_end(vr);

		int i = 0;
		//TODO ADD PROPER TEST
		//CURRENTLY WORKS
	}
	
	
}


