#pragma once
#include <fstream>
#include <string>
#include <vector>
#include <iostream>
namespace isam {

	template <class T>
	class BinaryStream
	{
		std::string filename;
		std::fstream file;

		bool is_position_valid(int pos);

	public:
		static int writes;
		static int reads;
		BinaryStream(std::string name);
		~BinaryStream();

		void set_file(std::string name);
		int get_size();
		void reset();
		void close();
		void open();
		void print();

		T read_at(int pos);
		void read_at(int pos, std::vector<T>& buffer);
		void write_at(int pos, T data);
		void write_at_end(T data);
		void write_at_end(std::vector<T>& buffer);
	};
	template <class T>
	int BinaryStream<T>::writes = 0;
	template <class T>
	int BinaryStream<T>::reads = 0;

	template <class T>
	BinaryStream<T>::BinaryStream(std::string name)
		:filename(name)
	{
		file.open(name.c_str(), std::ios::binary | std::ios::in | std::ios::out | std::ios::app);
		file.close();
		file.open(filename.c_str(), std::ios::binary | std::ios::in | std::ios::out);
	}

	template <class T>
	BinaryStream<T>::~BinaryStream()
	{
	}

	template <class T>
	void BinaryStream<T>::set_file(std::string name)
	{
		filename = name;
		file.open(name.c_str(), std::ios::binary | std::ios::in | std::ios::out | std::ios::app);
		file.close();
		file.open(filename.c_str(), std::ios::binary | std::ios::in | std::ios::out);
	}

	template <class T>
	void BinaryStream<T>::reset()
	{
		file.close();
		remove(filename.c_str());
		file.open(filename.c_str(), std::ios::binary | std::ios::in | std::ios::out | std::ios::app);
		file.close();
		file.open(filename.c_str(), std::ios::binary | std::ios::in | std::ios::out);
	}

	template <class T>
	void BinaryStream<T>::open()
	{
		file.close();
		file.open(filename.c_str(), std::ios::binary | std::ios::in | std::ios::out);
	}
	template <class T>
	void BinaryStream<T>::close()
	{
		file.close();
	}

	template <class T>
	void BinaryStream<T>::print()
	{
		for (int i = 0; i < this->get_size(); i++) {
			std::cout << i << '\t';
			this->read_at(i).print();
		}
	}

	template <class T>
	bool BinaryStream<T>::is_position_valid(int pos)
	{
		int max = get_size();
		if (pos > max)
			return false;

		if (pos < 0)
			return false;

		return true;
	}

	template <class T>
	int BinaryStream<T>::get_size()
	{
		file.seekg(file.beg, file.end);
		return (( file.tellg() )/sizeof(T));
	}

	template <class T>
	T BinaryStream<T>::read_at(int pos)
	{
		if (!is_position_valid(pos))
			throw std::exception("read_at, wrong position");

		file.seekg(file.beg);
		file.seekg(pos * sizeof(T));
		T ret;
		file.read(reinterpret_cast<char*>(&ret), sizeof(T));
		reads++;
		return ret;
	}

	template <class T>
	void BinaryStream<T>::read_at(int pos, std::vector<T>& buffer )
	{
		if (!is_position_valid(pos))
			throw std::exception("read_at, wrong position");

		file.seekg(file.beg);
		file.seekg(pos * sizeof(T));

		file.read(reinterpret_cast<char*>(buffer.data()), sizeof(T)* buffer.capacity() );
		reads++;
	}
	
	template <class T>
	void BinaryStream<T>::write_at(int pos, T data)
	{
		if (!is_position_valid(pos))
			throw std::exception("write_at, wrong position");

		file.seekp( pos*sizeof(T), std::ios::beg);
		writes++;
		file.write(reinterpret_cast<char*>(&data), sizeof(T));
		file.flush();
	}

	template <class T>
	void BinaryStream<T>::write_at_end(T data)
	{
		file.seekp(0, std::ios_base::end);
		writes++;
		file.write(reinterpret_cast<char*>(&data), sizeof(T));
		file.flush();
	}

	template <class T>
	void BinaryStream<T>::write_at_end(std::vector<T>& buffer)
	{
		file.seekp(0, std::ios_base::end);
		writes++;
		file.write(reinterpret_cast<char*>(buffer.data()), sizeof(T)*buffer.size());
		file.flush();
	}
};