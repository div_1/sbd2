#include "InputControl.h"

#include <vector>
#include <cmath>
#include "../Printer/Printer.h"
#include "../BinaryStream/BinaryStream.h"
#include "../DataRecord/DataRecord.h"
#include "../DataStream/DataStream.h"
#include "../DataOrganizer/DataOrganizer.h"
#include "../IndexStream/Index.h"
#include "../IndexStream/IndexRecord.h"

namespace isam {
	InputControl::InputControl()
	{
	}


	InputControl::~InputControl()
	{
	}

	void InputControl::create() 
	{
		remove("index_file.index");
		remove("data_file.data");
		BinaryStream<DataRecord> dataCreator("data_file.data");
		BinaryStream<IndexRecord> indexCreator("index_file.index");

		std::vector<DataRecord> vc(10);
		for (int i = 0; i < 10; i++)
			vc[i] = { 0,0,0,0,0 };

		dataCreator.write_at(0, { 0,0,0,0,0 }); //Initialize Sentinel
		dataCreator.write_at_end(vc);			//Initialize first page 
		indexCreator.write_at(0, { 0,1 });		//IndexRecord signalising empty file
	}
	void InputControl::add(DataRecord addee) 
	{
		{
			BinaryStream<IndexRecord> indexCreator("index_file.index");

			DataStream data("data_file.data");

			if (indexCreator.read_at(0).key == 0) {
				data.add_record(addee.key, addee.a, addee.b, addee.c, 1);
				indexCreator.write_at(0, { addee.key,1 });
			}
			else {
				int page = find_page("index_file.index", addee.key);
				data.add_record(addee.key, addee.a, addee.b, addee.c, page);
			}
		}
		auto_reorganize();
	}
	void InputControl::reorganize()
	{
		{
			DataOrganizer reorg("data_file.data");
			reorg.reorganize();
			reorg.flush();
		}
		data_to_index("data_file.data", "index_file.index");
	}

	void InputControl :: auto_reorganize()
	{
		int dsize;
		int isize;
		{
			BinaryStream<DataRecord> dataFile("data_file.data");
			BinaryStream<IndexRecord> indexFile("index_file.index");
			dsize = dataFile.get_size();
			isize = indexFile.get_size();
			dataFile.close();
			indexFile.close();
		}
		if( dsize > isize * 10 + std::log(isize*10)) {
			reorganize();
		}
	}


	void InputControl::print_data_sorted() {
		Printer printer("data_file.data");
		printer.print_sorted();

	}
	void InputControl::print_data_raw(){
		BinaryStream<DataRecord> printer("data_file.data");
		printer.print();
		
	}

	void InputControl::print_index() {
		BinaryStream<IndexRecord> printer("index_file.index");
		printer.print();
	}

	void InputControl::find(int key) {
		int page = find_page("index_file.index", key);
		DataStream data("data_file.data");
		DataRecord dr = data.find_record(page,key);

		if (dr.key == 0 && dr.b == 7)
			std::cout << "No Key Found" << std::endl;
		else{
			std::cout << "Key: " << dr.key << std::endl;
			std::cout << "A  : " << dr.a << std::endl;
			std::cout << "B  : " << dr.b << std::endl;
			std::cout << "C  : " << dr.c << std::endl;
			std::cout << "Ptr: " << dr.overflow << std::endl;
		}
	}
}
