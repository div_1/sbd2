#pragma once

#include <iostream>
#include <string>
#include "../DataRecord/DataRecord.h"

namespace isam {
	class InputControl
	{
		int overflow_pos;
		void auto_reorganize();
		void dispatch(std::string message, DataRecord rec = { 1,2,3,4,0 });

	public:
		InputControl();
		~InputControl();

		void create();
		void add(DataRecord addee);
		void reorganize();
		void print_data_raw();
		void print_data_sorted();
		void print_index();
		void find(int key);
	};
}

