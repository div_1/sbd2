#include <iostream>
#include <string>
#include "InputControl\InputControl.h"
#include "BinaryStream\BinaryStream.h"
#include "DataRecord/DataRecord.h"
#include "IndexStream\IndexRecord.h"
#include <fstream>


void load_stats() {
	std::fstream file;
	file.open("stats.meta", std::ios::binary | std::ios::in);
	int stats[4] = { 0,0 };
	file.seekg(std::ios::beg);
	file.read(reinterpret_cast<char*>(stats), sizeof(int) * 4);
	isam::BinaryStream<isam::DataRecord>::writes = stats[0];
	isam::BinaryStream<isam::DataRecord>::reads = stats[1];
	isam::BinaryStream<isam::IndexRecord>::writes = stats[2];
	isam::BinaryStream<isam::IndexRecord>::reads = stats[3];
	file.close();
	remove("stats.meta");

}
void save_stats() {
	std::fstream file;
	file.open("stats.meta", std::ios::binary | std::ios::app);
	int stats[4] = { 0,0 };
	stats[0] = isam::BinaryStream<isam::DataRecord>::writes;
	stats[1] = isam::BinaryStream<isam::DataRecord>::reads;
	stats[2] = isam::BinaryStream<isam::IndexRecord>::writes;
	stats[3] = isam::BinaryStream<isam::IndexRecord>::reads;
	file.write(reinterpret_cast<char*>(stats), sizeof(int) * 4);
	file.close();
}

int main(int argc, char* argv[], char* env) {
	load_stats(); 
	int temp_stats[4];
	temp_stats[0] = isam::BinaryStream<isam::DataRecord>::writes;
	temp_stats[1] = isam::BinaryStream<isam::DataRecord>::reads;
	temp_stats[2] = isam::BinaryStream<isam::IndexRecord>::writes;
	temp_stats[3] = isam::BinaryStream<isam::IndexRecord>::reads;
	isam::InputControl ctrl;

	if (argc > 1) {
		if (std::string(argv[1]) == std::string("--create")) {
			isam::BinaryStream<isam::DataRecord>::writes = 0;
			isam::BinaryStream<isam::DataRecord>::reads = 0;
			isam::BinaryStream<isam::IndexRecord>::writes = 0;
			isam::BinaryStream<isam::IndexRecord>::reads = 0;
			remove("stats.meta");
			ctrl.create();

		}
		else {
			if (std::string(argv[1]) == std::string("--add")) {
				if (argc > 2 && std::string(argv[2]) == std::string("repeat")) {
					if (argv[3] != 0)
						ctrl.add({ atoi(argv[3]),
							static_cast<float>(atof(argv[3])),
							static_cast<float>(atof(argv[3])),
							static_cast<float>(atof(argv[3])),0 });
				}
				else if (argc > 5) {
					if (argv[2] != 0)
						ctrl.add({ atoi(argv[2]),
							static_cast<float>(atof(argv[3])),
							static_cast<float>(atof(argv[4])),
							static_cast<float>(atof(argv[5])),0 });
				}
			}
			if (1 && std::string(argv[1]) == std::string("--print")) {
				if (argc > 2 && std::string(argv[2]) == std::string("data")) {
					if (argc > 3 && std::string(argv[3]) == std::string("raw"))
						ctrl.print_data_raw();
					if (argc > 3 && std::string(argv[3]) == std::string("sorted"))
						ctrl.print_data_sorted();
				}
				if (argc > 2 && std::string(argv[2]) == std::string("index"))
					ctrl.print_index();
			}
			if (std::string(argv[1]) == std::string("--find")) {
				ctrl.find(atoi(argv[2]));
			}
			if (std::string(argv[1]) == std::string("--reorganize")) {
				ctrl.reorganize();
			}
			if (std::string(argv[1]) == std::string("--stats")) {
				if (argc > 2 && std::string(argv[2]) == std::string("data")) {
					std::cout << "Data file writes: " << isam::BinaryStream<isam::DataRecord>::writes << std::endl;
					std::cout << "Data file reads : " << isam::BinaryStream<isam::DataRecord>::reads << std::endl;
				}
				if (argc > 2 && std::string(argv[2]) == std::string("index")) {
					std::cout << "Index file writes: " << isam::BinaryStream<isam::IndexRecord>::writes << std::endl;
					std::cout << "Index file reads : " << isam::BinaryStream<isam::IndexRecord>::reads << std::endl;
				}
			}
			std::cout << "Data file writes: " << isam::BinaryStream<isam::DataRecord>::writes - temp_stats[0] << std::endl;
			std::cout << "Data file reads: " << isam::BinaryStream<isam::DataRecord>::reads - temp_stats[1] << std::endl;
			std::cout << "Index file writes: " << isam::BinaryStream<isam::IndexRecord>::writes - temp_stats[2] << std::endl;
			std::cout << "Index file reads: " << isam::BinaryStream<isam::IndexRecord>::reads - temp_stats[3] << std::endl;
		}
	}


	save_stats();
	return 0;
}